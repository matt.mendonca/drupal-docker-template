## Install
- `cd app`
- `composer install`
- `cd ../`
- `docker-compose stop; docker-compose rm -f; docker-compose up --build`

See https://docker4drupal.readthedocs.io/en/latest/